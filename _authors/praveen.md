---
layout: author
username: praveen
name_ml: പ്രവീൺ അരിമ്പ്രത്തൊടിയിൽ
---
A skilled labourer in a social emergency campaigning to remake this world as he sees fit (a pirate). A world where everyone have enough to survive and can do what they really love to do as opposed to what pay them the most. "The acquisition of wealth is no longer the driving force in our lives. We work to better ourselves and the rest of humanity." - Jean Luc Picard, Star Trek First Contact.
