---
layout: series
authors:
  - subinpt
name: file-system
name_ml: ലിനക്സ് ഫയല്‍ സിസ്റ്റങ്ങള്‍
---

ലിനക്സിലെ വിവിധ ഫയല്‍ സിസ്റ്റങ്ങള്‍ പരിചയപ്പെടുത്തുന്ന ഒരു പരമ്പര.
