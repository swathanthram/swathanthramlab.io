---
layout: series
authors:
  - subinpt
name: linux-kernel
name_ml: ലിനക്സ് കേണല്‍
---

എന്താണ് ലിനക്സ് കേണല്‍, എങ്ങിനെയാണ് ലിനക്സ് കേണല്‍ പ്രവര്‍ത്തിക്കുന്നതു് എന്നു് തുടങ്ങി കേണല്‍ സോഴ്സ്കോഡ് സ്വയം കമ്പൈയ്ല്‍ ചെയ്തു് ഉപയോഗിയ്ക്കാന്‍ പ്രാപ്തമാക്കുന്നു.