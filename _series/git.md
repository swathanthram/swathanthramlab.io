---
layout: series
authors:
  - balasankarc
name: git
name_ml: ഗിറ്റ്
---

വികേന്ദ്രീകൃത പതിപ്പ് കൈകാര്യ ഉപകരണമായ ഗിറ്റിനെ പരിചയപ്പെടുത്തുന്ന ഒരു പരമ്പര.
