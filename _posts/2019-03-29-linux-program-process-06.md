---
layout: post
title: "പ്രോഗ്രാം, പ്രോസസ്സ് #6"
author: subinpt
series: program-process
Tags: പ്രോഗ്രാം, പ്രോസസ്സ്
---

പ്രോസസ്സുകളെക്കുറിച്ചുള്ള വിവരങ്ങള്‍ രണ്ട് വിഭാഗങ്ങളിലായി ആണ് സൂക്ഷിക്കപ്പെട്ടിരിക്കുന്നത്. പ്രോസസ്സ് ടേബിളും യു-ഏരിയയും. പ്രോസസ്സിനെക്കുറിച്ച് കെര്‍ണലിന് ആവശ്യമുള്ള വിവരങ്ങള്‍ പ്രോസസ്സ് ടേബിളില്‍ സൂക്ഷിച്ചിരിക്കും. പ്രോസസ്സിന് തന്നെ ആവശ്യമുള്ള വിവരങ്ങള്‍ യു-ഏരിയയിലും സൂക്ഷിച്ചിരിക്കും.

## പ്രോസസ്സ് ടേബിള്‍ എന്‍ട്രി
കെര്‍ണല്‍ സൂക്ഷിക്കുന്ന പ്രോസസ്സുകളുടെ പട്ടികയാണ് പ്രോസസ്സ് ടേബിള്‍. കെര്‍ണല്‍ എങ്ങനെയാണ് പ്രോസസ്സുകളെ കൈകാര്യം ചെയ്യുന്നത് എന്ന് മനസ്സിലാക്കാന്‍ പ്രോസസ്സ് ടേബിളിനെക്കുറിച്ചൂള്ള വിവരങ്ങള്‍ സഹായകമായിരിക്കും.

പ്രോസസ്സ് ടേബിള്‍ ഒരു സര്‍ക്കുലാര്‍ ഡബ്ലി ലിങ്ക്ഡ് ലിസ്റ്റ് ആണ്. (`Circular Doubly Linked List`). ഇതില്‍ ഏത് പ്രോസസ്സിന്റെ എന്‍ട്രിയില്‍ നിന്ന് ആരംഭിച്ചാലും മുന്നിലേക്കോ പിന്നിലേക്കോ പോയി എല്ലാ പ്രോസസ്സുകളുടെയും  എന്‍ട്രികളിലൂടെ പോയി വരാന്‍ സാധിക്കും. ഒരു പ്രോസസ്സിന്റെ പ്രോസസ്സ് ടേബിള്‍ എന്‍ട്രിയില്‍ ഉള്ള വിവരങ്ങളില്‍ ചിലത് താഴെക്കൊടുക്കുന്നു.

1. പ്രോസസ്സിന്റെ അവസ്ഥ. യൂണിക്സിലെ വിവിധ പ്രോസസ്സ് അവസ്ഥകള്‍ വിശദമാക്കുന്ന ചിത്രം താഴെക്കൊടുത്തിരിക്കുന്നു. ![വിക്കീപീടിയയ്ക്കു് കടപ്പാട്](http://1.bp.blogspot.com/-okt_LDm3Xtc/UIePzzX-pHI/AAAAAAAAB78/UB4rEHr0IcQ/s400/Unix_process_states.png)
2. പ്രോസസ്സ് ഐഡി
3. വിവിധ യൂസര്‍ ഐഡികള്‍ - ഇവ ഉപയോഗിച്ചാണ് ഒരു പ്രോസസ്സിനുള്ള അനുമതികള്‍ തീരുമാനിക്കപ്പെടുന്നത്.
4. പ്രോസസ്സിലും ഷെയേര്‍ഡ് ലൈബ്രറികളിലുമായി ഉള്ള ടെക്സ്റ്റ് സെഗ്‌‌മെന്റുകളിലേക്കുള്ള പോയിന്ററുകള്‍.
5. മെമ്മറി മാനേജ്‌‌മെന്റ് ആവശ്യങ്ങള്‍ക്കായി പേജ് ടേബിളിലേക്കുള്ള പോയിന്ററുകള്‍.
6. ഷെഡ്യൂളിങ്ങിന് ആവശ്യമായ വിവരങ്ങള്‍. ലിനക്സ് സിസ്റ്റങ്ങളില്‍ പ്രോസസ്സുകള്‍ക്ക് 'നൈസ് വാല്യു' എന്നറിയപ്പെടുന്ന മൂല്യങ്ങള്‍ നല്‍കിയിരിക്കും. പ്രോസസ്സ് ഷെഡ്യൂളിങ്ങിന്റെ സമയത്ത് ഏതൊക്കെ പ്രോസസ്സുകള്‍ക്ക് പ്രാധാന്യം നല്‍കണം എന്ന് തീരുമാനിക്കപ്പെടുന്നത് ഈ മൂല്യത്തിന്റെ അടിസ്ഥാനത്തിലാണ്.
7. വിവിധ ടൈമറുകള്‍. ഓരോ പ്രോസസ്സിനും പ്രവര്‍ത്തിക്കാന്‍ സി പി യു വില്‍ സമയ പരിധികള്‍ തീരുമാനിക്കപ്പെട്ടിരിക്കും. ഈ പരിധികള്‍ പാലിക്കാനും പ്രോസസ്സ് എത്ര സമയം ഏതൊക്കെ മോഡില്‍ പ്രവര്‍ത്തിച്ചു എന്നൊക്കെ കണക്കാക്കാനും ഈ ടൈമറുകള്‍ ഉപയോഗിക്കപ്പെടുന്നു.
8. പ്രോസസ്സിന്റെ യു-ഏരിയയിലേക്കുള്ള പോയിന്റര്‍.

## യു-ഏരിയ
പ്രോസസ്സിന്റെ യു-ഏരിയയില്‍ ഉള്‍പ്പെടുത്തിയിരിക്കുന്ന കാര്യങ്ങള്‍ ചുവടെ ചേര്‍ക്കുന്നു.
1. റിയല്‍ യൂസര്‍ ഐഡി, എഫക്റ്റീവ് യൂസര്‍ ഐഡി.
2. പ്രോഗ്രാമിന്റെ വര്‍ക്കിങ്ങ് ഡയറക്ടറി (ഒരു പ്രോഗ്രാം പ്രവര്‍ത്തിക്കുന്നത് ഒരു സമയത്ത് ഫയല്‍ സിസ്റ്റത്തിലെ ഏതെങ്കിലും  ഒരു ഡയറക്ടറിയില്‍ ആയിരിക്കും. ഇതിന് പ്രോഗ്രാമുമായി നേരിട്ട് ബന്ധമൊന്നുമില്ലെങ്കിലും ആ ഡയറക്ടറിയില്‍ ഉള്ള ഫയലുകള്‍ ഉപയോഗിക്കാന്‍ മുഴുവന്‍ പാത്ത് നല്‍കാതെ ഫയലിന്റെ പേര് മാത്രം ഉപയോഗിച്ചാല്‍ മതിയാകും)
3. യൂസര്‍ മോഡിലും  കെര്‍ണല്‍ മോഡിലുമായി പ്രോഗ്രാം  ചെലവിട്ട സമയം കണക്കാക്കുന്നതിനായുള്ള ടൈമറുകള്‍.
4. പ്രോഗ്രാം സിഗ്നലുകളോട് എങ്ങനെ പ്രതികരിക്കണം എന്നതിനെ സംബന്ധിച്ച വിവരങ്ങള്‍.
5. പ്രോഗ്രാമുമായി ബന്ധപ്പെട്ടിരിക്കുന്ന കണ്ട്രോളിങ്ങ് ടെര്‍മിനലിനെ സംബന്ധിച്ച വിവരങ്ങള്‍.
6. പ്രോഗ്രാം നടത്തിയ സിസ്റ്റം കോളൂകളെ സംബന്ധിച്ച വിവരങ്ങള്‍.
7. യൂസര്‍ ഫയല്‍ ഡിസ്ക്രിപ്റ്റര്‍ ടേബിള്‍ - ഒരു പ്രോഗ്രാം തുറക്കുന്ന ഫയലുകളുടെ ഡിസ്ക്രിപ്റ്ററുകളുടെ പട്ടിക.
8. ഫയല്‍ സിസ്റ്റത്തെയും പ്രോസസ്സ് എന്‍വയോണ്‍മെന്റിനെയും സംബന്ധിച്ച വിവരങ്ങള്‍യ്

## യൂസര്‍ ഐഡികള്‍
ഫയലുകള്‍ക്ക് ഉള്ളത് പോലെ തന്നെ പ്രോസസ്സുകള്‍ക്കും യൂസറും ഗ്രൂപ്പും ഒക്കെ ഉണ്ട്. ഇത് കൂടാതെ സെഷനും. പ്രോസസ്സുകള്‍ക്കുള്ള വിവിധ യൂസര്‍/ഗ്രൂപ്പ് ഐഡികള്‍ എന്നിവ ചുവടെ. സിസ്റ്റത്തില്‍ ഉള്ള ഓരോ ഉപയോക്താവിന്റെയും ഗ്രൂപ്പിന്റെയും പേരുകള്‍ ഒരു യൂസര്‍ ഐഡിയും ഗ്രൂപ്പ് ഐഡിയും ആയി ബന്ധപ്പെട്ടിരിക്കുന്നു. എല്ലാ സമയത്തും മുഴുവന്‍ പേരും ഉപയോഗിക്കാനുള്ള വിഷമം പരിഗണിച്ചാണ് ഓരോ പേരുകളുമായി ബന്ധപ്പെട്ട് സംഘ്യകള്‍ നല്‍കിയിരിക്കുന്നത്. ഇതിനെപ്പറ്റിയുള്ള വിവരങ്ങള്‍ അടങ്ങിയിരിക്കുന്നത് `/etc/passwd`, `/etc/group` എന്നീ ഫയലുകളില്‍ ആണ് ഉണ്ടായിരിക്കുക.

## റിയല്‍ യൂസര്‍ ഐഡി
ഇത് ഒരു പ്രോസസ്സ് തുടങ്ങിവച്ച ഉപയോക്താവിന്റെ ഐഡി ആയിരിക്കും. ഒരു പ്രോസസ്സ് ആരംഭിച്ച ശേഷം സെറ്റ്‌‌യുഐഡി വഴി അതിന്റെ യൂസര്‍ ഐഡി മാറ്റിയേക്കാം. എന്നാലും റിയല്‍ യൂസര്‍ ഐഡി പഴയത് തന്നെ ആയിരിക്കും. ഈ പ്രോസസ്സ് ഉണ്ടാക്കുന്ന ഫയലുകള്‍ക്കോ പ്രോസസ്സിന്റെ അനുമതികള്‍ക്കോ റിയല്‍ യൂസര്‍ ഐഡി ഉപയോഗിക്കാറില്ല. എന്നാല്‍ മറ്റ് പ്രോസസ്സുകള്‍ക്ക് സിഗ്നലുകള്‍ അയക്കുമ്പോള്‍ പരിഗണിക്കുന്നത് റിയല്‍ യൂസര്‍ ഐഡി ആണ്‍. സൂപ്പര്‍യൂസര്‍ അനുമതികളില്ലാത്ത ഒരു പ്രോസസ്സിന് അതിന്റെ റിയല്‍ യൂസര്‍ ഐഡി തന്നെയുള്ള പ്രോസസ്സുകള്‍ക്കേ സിഗ്നലുകള്‍ അയക്കാന്‍ സാധിക്കുകയുള്ളു.

## എഫക്റ്റീവ് യൂസര്‍ ഐഡി
ഒരു പ്രോസസ്സ് സൃഷ്ടിക്കപ്പെട്ട ശേഷം സെറ്റ്‌‌യുഐഡി ഉപയോഗിച്ച് ആ പ്രോസസ്സിന്റെ എഫക്റ്റീവ് യൂസര്‍ ഐഡി റിയല്‍ യൂസര്‍ ഐഡിയില്‍ നിന്ന് മാറ്റാന്‍ സാധിക്കും. ഒരു പ്രോസസ്സ് ഫയലുകള്‍ നിര്‍മ്മിക്കുകയോ ഏതെങ്കിലും  ഒരു റിസോഴ്സ് ഉപയോഗിക്കാന്‍ ശ്രമിക്കുകയോ ചെയ്യുമ്പോള്‍ ആ പ്രോസസ്സിന്റെ എഫക്റ്റീവ് യൂസര്‍ ഐഡി ആണ് പരിഗണിക്കപ്പെടുക.

ഗ്രൂപ്പ് ഐഡികളും മേല്‍പ്പറഞ്ഞത് പോലെ തന്നെ. യൂസറിന് പകരം യൂസര്‍ ഉള്‍പ്പെടുന്ന ഗ്രൂപ്പിന്റെ ഐഡി ആയിരിക്കും പരിഗണിക്കപ്പെടുക. താഴെക്കൊടുത്തിരിക്കുന്ന പ്രോഗ്രാം പ്രോസസ്സിന്റെ യൂസര്‍ ഐഡി, ഗ്രൂപ്പ് ഐഡി തുടങ്ങിയവ കാണിച്ചു തരും. `sudo` ഉപയോഗിക്കാതെ റണ്‍ ചെയ്താല്‍ `setuid` പ്രവര്‍ത്തിക്കില്ല. `65534` nobody എന്ന യൂസറിന്റെ ഐഡി ആണ്. ലഭ്യമായ യൂസര്‍ ഐഡികള്‍ക്കായി `/etc/passwd` ഫയല്‍ തുറന്ന് നോക്കുക. സെറ്റ്‌‌യുഐഡി റിയല്‍ യൂസര്‍ ഐഡി മാറ്റുന്നില്ല എന്ന് ഈ പ്രോഗ്രാം  പ്രവര്‍ത്തിക്കുമ്പോള്‍ മനസ്സിലാക്കാം.
```c
#include <stdio.h>  
#include <unistd.h>  

int main(void)   
{  
    printf("The real user of this process has ID %d\n",getuid());  
    printf("The effective user of this process has ID %d\n",geteuid());  
    printf("The real user group ID for this process is %d\n",getgid());  
    printf("The effective user group for this process is %d\n",getegid());  

    setuid(65534);  

    printf("The real user of this process has ID %d\n",getuid());  
    printf("The effective user of this process has ID %d\n",geteuid());  
    printf("The real user group ID for this process is %d\n",getgid());  
    printf("The effective user group for this process is %d\n",getegid());  

    return 0;  
}
```
## പ്രോസസ്സ് സെഷന്‍
എല്ലാ പ്രോസസ്സുകളും  ഒരു പ്രോസസ്സ് സെഷന്റെ ഭാഗമായിരിക്കും. മിക്കവാറും സിസ്റ്റം പ്രവര്‍ത്തിച്ച് തുടങ്ങുമ്പോള്‍ ഉള്ള ലോഗിന്‍ പ്രോസസ്സ് ആയിരിക്കും ഒരു സെഷന്‍ ആരംഭിക്കുക. അതിന്റെ ചൈല്‍ഡ് പ്രോസസ്സുകള്‍ ഒക്കെ ആ സെഷനില്‍ ആയിരിക്കും. ഒരു സെഷനിലെ ആദ്യത്തെ പ്രോസസ്സ് ആണ് സെഷന്‍ ലീഡര്‍ എന്നറിയപ്പെടുന്നത്. സെഷന്‍ ഐഡി സെഷന്‍ ലീഡറിന്റെ പിഐഡി ആയിരിക്കും. എല്ലാ പ്രോസസ്സ് സെഷനുകള്‍ക്കും ഒരു കണ്ട്രോളിങ്ങ് റ്റിറ്റിവൈ ഉണ്ടായിരിക്കും. ഇതിനെ ടെര്‍മിനല്‍ എന്ന് വിളിക്കാം. ആദ്യകാലത്ത് ഒരു പ്രധാന കമ്പ്യൂട്ടറുമായി ഘടിപ്പിക്കപ്പെട്ടിരുന്ന ഉപകരണങ്ങളായിരുന്നു ടെര്‍മിനലുകള്‍. ഒന്നിലധികം ടെര്‍മിനലുകള്‍ ഉപയോഗിച്ച് ഒന്നിലധികം ഉപയോക്താക്കള്‍ ഒരേ കമ്പ്യൂട്ടര്‍ തന്നെ പ്രവര്‍ത്തിപ്പിച്ചിരുന്നു. ps -e കമാന്റ് ഉപയോഗിച്ച് സിസ്റ്റത്തിലെ പ്രോസസ്സുകളും അവയുടെ TTY ഉം  കാണാന്‍ സാധിക്കും. ചില പ്രോസസ്സുകള്‍ക്ക് കണ്ട്രോളിങ്ങ് ടെര്‍മിനല്‍ കാണില്ല. അവയെക്കുറിച്ച് പിന്നീട് പറയാം. താഴെക്കൊടുത്തിരിക്കുന്ന പ്രോഗ്രാം അതിന്റെ സെഷന്‍ ഐഡി കാട്ടിത്തരും. അതിന് ശേഷം ps കമാന്റ് ഉപയോഗിച്ചാല്‍ ആ പ്രോഗ്രാമിനെ പ്രവര്‍ത്തിപ്പിച്ച ഷെല്ലിന്റെ പിഐഡി തന്നെയാണ് സെഷന്‍ ഐഡി എന്ന് കാണാം. അതിന്റെ കാരണം ഊഹിക്കാമല്ലോ.

```c
#include <stdio.h>  
#include <stdlib.h>  
#include <unistd.h>  

int main(void)  
{  
    printf("My session id is: %d\n", getsid(getpid()));  

    return 0;  
}  
```

##### ഈ ലേഖനം ശ്രീ [സുബിന്‍ പി. റ്റി](/authors/subin) അദ്ദേഹത്തിന്റെ [ബ്ലോഗില്‍](http://mymalayalamlinux.blogspot.com/) ക്രിയേറ്റീവ് കോമണ്‍സ് [സിസി-ബൈ-എസ്.എ 3.0](https://creativecommons.org/licenses/by-sa/3.0/deed.en_US) ലൈസന്‍സ് പ്രകാരം പ്രസിദ്ധീകരിച്ചതാണ്.
